package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> allItems = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return allItems.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() < totalSize()){
            allItems.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(allItems.contains(item)){
            allItems.remove(item);
        }
        else{
            throw new NoSuchElementException("Fridge does not contain such item.");
        }

    }

    @Override
    public void emptyFridge() {
        allItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<>();
        for (FridgeItem fridgeItem: allItems) {
            if(fridgeItem.hasExpired()){
                expired.add(fridgeItem);
            }
        }
        for (FridgeItem expiredItem: expired){
            takeOut(expiredItem);
        }
        return expired;
    }
}
